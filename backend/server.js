/* npm modules */
var express = require('express');
var app = express();
var http = require('http');
var nodemailer = require('nodemailer');
var querystring = require('querystring');

/* Static routes */
app.use('/', express.static('../frontend'));
//app.use('/data', express.static('data'));

/* Dynamic routes */
app.get('/contact', function(req, res) {
    console.log('Received message from contact form.');

    /* Creating the mail */
    var content = "From: " + req.query.name + ' <' + req.query.mail + '>' + '\n' + "Message: " + req.query.message;

    /* Sending the mail */
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'paul.selle94@gmail.com',
            pass: 'ykuavjxyychhmfnz'
        }
    });

    var mailOptions = {
        from: 'paul.selle.94@gmail.com',
        to: 'paul.selle.94@gmail.com',
        subject: 'New email from my website',
        text: content
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            res.sendStatus(200);
        }
    });
});

app.get('/chat', function(req, res) {
    var message = req.query.message;
    var answer = "I will soon have a trained neural network in my brain to answer this relevant question.";
    var messageData = querystring.stringify({'message': message});
    console.log(messageData);
    var options = {
        host: '127.0.0.1',
        port: '8080',
        path: '/paulbot?' + messageData,
        method: 'GET'
    };
    const reqFlask = http.request(options, function(resFlask) {
        console.log('http request sent to Flask.');

        resFlask.on('data', (chunk) => {
            console.log('Received the following answer from the Flask server: ' + chunk + '.');
            answer = chunk;
            res.send(answer);
        });
        resFlask.on('end', () => {
            console.log('No more data in response.');
        });
    });

    reqFlask.on('error', (e) => {
        console.error('Problem with request: ' + e.message + '.');
    });

    reqFlask.write(messageData);
    reqFlask.end();

    //res.send(answer);
});

/* Server startup */
var port = 80;
var server = http.createServer(app);
server.listen(port, "0.0.0.0"); // Localhost
console.log('Server started on port ' + port + '.');
