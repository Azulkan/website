# coding: utf-8

import aiml
import sys
from flask import Flask, jsonify, request, render_template

# The Kernel object is the public interface to
# the AIML interpreter.
k = aiml.Kernel()

# Use the 'learn' method to load the contents
# of an AIML file into the Kernel.
k.learn("std-startup.xml")

# Use the 'respond' method to compute the response
# to a user's input string.  respond() returns
# the interpreter's response, which in this case
# we ignore.
k.respond("load aiml b")

# Loop forever, reading user input from the command
# line and printing responses.
#while True: print k.respond(raw_input("> "))

app = Flask("__name__")

@app.route("/")
def kikoo():
    return "It works !"

@app.route("/paulbot", methods=["GET"])
def paulbot():
    answer = request.args.get('message')
    print('Received following answer from node server: ' + answer, file=sys.stdout)
    return k.respond(answer)

if __name__ == "__main__":
    app.run(port = 8080)
