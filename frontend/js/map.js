function myMap() {
    var mapOptions = {
        center: new google.maps.LatLng(49.44, 1.1),
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}
