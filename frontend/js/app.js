$(document).ready(function() {
    $(window).scroll(function () {
	if($(window).width() >= 768) {
		if ($(window).scrollTop() > 50) {
		    $('.navbar').addClass('navbar-fixed');
		}
		if ($(window).scrollTop() < 51) {
		    $('.navbar').removeClass('navbar-fixed');
		}
	}
    });

    $("#enter-website").click(function() {
        $(".container").fadeOut(1000);
        setTimeout(function() {
            window.location.href = "main.html"
        }, 1000);

    })

    $("#fb").hover(
        function() {
            $(this).attr("src", "img/fb-b.png");
        },
        function() {
            $(this).attr("src", "img/fb.png");
        }
    )

    $("#skype").hover(
        function() {
            $(this).attr("src", "img/skype-b.png");
        },
        function() {
            $(this).attr("src", "img/skype.png");
        }
    )

    $("#linkedin").hover(
        function() {
            $(this).attr("src", "img/linkedin-b.png");
        },
        function() {
            $(this).attr("src", "img/linkedin.png");
        }
    )

    $("#gitlab").hover(
        function() {
            $(this).attr("src", "img/gitlab-b.png");
        },
        function() {
            $(this).attr("src", "img/gitlab.png");
        }
    )

    $("#chat-text-input").keydown(function(e) {
        if(e.which == 13) { // Enter
            sendChat();
        }
    })

    $("#en-flag").click(function() {
        $("#id-text h1").text("Hello,");
        $("#id-text h2").text("I am Paul Selle.");

        $($(".nav.navbar-nav li a").get(0)).text("Home");
        $($(".nav.navbar-nav li a").get(1)).text("Summary");
        $($(".nav.navbar-nav li a").get(2)).text("Competencies");
        $($(".nav.navbar-nav li a").get(3)).text("Contact");

        $("#summary h2").text("A few things about me...");
        $($("#summary span p").get(0)).text("I am 22 years old and live in Rouen, France.");
        $($("#summary span p").get(1)).text("I am currently in my last year of engineering school at INSA Rouen Normandie, where I study IT systems architecture.");
        $($("#summary span p").get(2)).text("I am passionate with new technologies and computer science. I believe AI and algorithms will shape our world for the years to come and this makes me very enthusiastic.");
        $($("#summary span p").get(3)).text("One of my hobby is playing video games. I think this passion got me closer to softwares and computer science.");

        $("#dl-resume").text("Download my resume");

        $($("#switch-div td").get(0)).text("Computer science");

        $("#contact h1").text("Contact me");
        $("#contact p").text("You may have landed on this page because I applied for an internship. If you find my profile interesting, feel free to email me so we can get in touch.");
        $("#name").attr("placeholder", "Your name");
        $("#mail").attr("placeholder", "Your email");
        $("#message").attr("placeholder", "Your message");
        $("#send").text("Send");
    })

    $("#fr-flag").click(function() {
        $("#id-text h1").text("Bonjour,");
        $("#id-text h2").text("Je suis Paul Selle.");

        $($(".nav.navbar-nav li a").get(0)).text("Accueil");
        $($(".nav.navbar-nav li a").get(1)).text("Résumé");
        $($(".nav.navbar-nav li a").get(2)).text("Compétences");
        $($(".nav.navbar-nav li a").get(3)).text("Contact");

        $("#summary h2").html("&Agrave propos de moi...");
        $($("#summary span p").get(0)).text("J'ai 22 ans et j'habite à Rouen, France.");
        $($("#summary span p").get(1)).html("Je suis en dernière année d'études d'Ingénieur à l'<a href='http://www.insa-rouen.fr/'>INSA Rouen Normandie</a>, où j'étudie dans le département <a href='http://asi.insa-rouen.fr/'> Architecture des Systèmes d'Information</a>.");
        $($("#summary span p").get(2)).text("Je suis passionné par l'informatique et les nouvelles technologies. J'ai la conviction que l'IA et les algorithmes vont façonner le monde de demain et cela me rend très enthousiaste.");
        $($("#summary span p").get(3)).text("Un de mes hobbies est le jeu vidéo. Je pense que cette passion m'a rapproché de l'informatique et des logiciels.");

        $("#dl-resume").text("Télécharger mon CV");

        $($("#switch-div td").get(0)).text("Développement logiciel");

        $("#contact h1").text("Me contacter");
        $("#contact p").text("Vous avez sans doute atterri sur cette page parce que j'ai candidaté à un stage. Si vous trouvez mon profil intéressant, n'hésitez pas à m'envoyer un message pour que nous entrions en contact.");
        $("#name").attr("placeholder", "Votre nom");
        $("#mail").attr("placeholder", "Votre email");
        $("#message").attr("placeholder", "Votre message");
        $("#send").text("Envoyer");
    })
})

/* Contact form */
function handleContactForm() {
    console.log('Sending email to the server...');
    var name = $("#name").val();
    var mail = $("#mail").val();
    var message = $("#message").val();

    if(name != '' && mail != '' && message != '') {
        $.ajax({
            url: 'contact',
            type: 'GET',
            data: {
                name: name,
                mail: mail,
                message: message
            },
            success: function() {
                console.log('Message sent to the server.');
            },
            error: function() {
                console.log('Message could not be sent to the server.');
            }
        });

        $("#contact-form").html("Your message has been sent, thank you. I will reply ASAP.").css({
            "text-align": "center",
            "color": "#38a68b",
            "font-size": "18px"
        });
    } else {
        alert("Wrong input !");
    }
}
