$(document).ready(function() {
    $("#bot-avatar").click(function(e) {
        $("#bot-avatar").fadeOut(400);
        $("#chat").fadeIn(400);
    });

    initBot();

    $(document).mouseup(function(e) {
        var chat = $("#chat");
        var botToggler = $("#bot-toggler");
        // if the chat is displayed & if the target of the click isn't the chat nor a descendant of the chat
        if ($("#chat").css("display") == "block" && !botToggler.is(e.target) && !chat.is(e.target) && chat.has(e.target).length === 0) {
            chat.fadeOut(400);
            $("#bot-avatar").fadeIn(600);
        }
    });
});

function initBot() {
    $("#bot-greeting").fadeIn(1000);
    setTimeout(function() {
        $("#bot-greeting").fadeOut(1000);
    }, 8000);

    setInterval(function() {
        $("#bot-avatar").css("background-image", "url(img/avatar3.png)");
        setTimeout(function() {
            $("#bot-avatar").css("background-image", "url(img/avatar4.png)");
            setTimeout(function() {
                $("#bot-avatar").css("background-image", "url(img/avatar2.png)");
            }, 500);
        }, 500)
    }, 10000);

    $("#close-bubble").click(function() {
        $("#bot-greeting").fadeOut(1000);
    });
}

function toggleBot() {
    if($("#bot-avatar").css("display") === "none")
        $("#bot-avatar").css("display", "block");
    else if($("#bot-avatar").css("display") === "block") {
        $("#bot-avatar").css("display", "none");
        $("#bot-greeting").css("display", "none");
    }
};

function sendChat() {
    var message = $("#chat-text-input").val();
    if(message != '') {
        /* Add the message to the chat */
        $("#chat-msgs").append("<div class='msg-div'><div class='msg-text user-msg'><span>" + message + "</span></div></div>");

        /* Scroll the chat to the last message */
        var messageList = $("#chat-msgs")[0];
        messageList.scrollTop = messageList.scrollHeight;

        /* Clear input */
        $("#chat-text-input").val("");

        /* Send the message to the server */
        $.ajax({
            url: 'chat',
            type: 'GET',
            data: {
                message: message
            },
            success: function(data) {
                /* Write the bot's answer */
                $("#chat-msgs").append("<div class='msg-div'><div class='msg-text bot-msg'><span>" + data + "</span></div><img class='bot-pic' src='img/avatar-small.png'/></div>");

                /* Scroll the chat to the last message */
                var messageList = $("#chat-msgs")[0];
                messageList.scrollTop = messageList.scrollHeight;
            },
            error: function() {
                console.log('Message could not be send to the bot.');
            }
        });
    }
}
