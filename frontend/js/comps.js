$(document).ready(function() {
    $('#slider-java').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 70]
    });

    $('#slider-cpp').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 40]
    });

    $('#slider-c').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 60]
    });

    $('#slider-python').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 70]
    });

    $('#slider-matlab').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 70]
    });

    $('#slider-bash').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 40]
    });

    $('#slider-html').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 95]
    });

    $('#slider-css').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 80]
    });

    $('#slider-js').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 80]
    });

    $('#slider-php').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 45]
    });

    $('#slider-postgresql').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 40]
    });

    $('#slider-design').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 75]
    });

    $('#slider-android').slider({
        orientation: 'horizontal',
        range:       true,
        values:      [0, 75]
    });

    /* Prevent user from modifying the value */
    $('.flat-slider').on("slide", function (event, ui) { return false; });
})
