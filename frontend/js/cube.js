$(document).ready(function() {
    var angle = 0;
    $("#flip, .front, .back, #switch").click(function() {
        //$(".flip-container").toggleClass('flip');
        angle += 180
        $(".flipper").css("transform", "rotateY(" + angle + "deg)");
        $("#switch").css("transform", "rotateY(" + angle + "deg)");
    });
});
